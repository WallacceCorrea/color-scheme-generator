//Predefined color modes to the API query
const modesEnum = [
  "monochrome",
  "monochrome-dark",
  "monochrome-light",
  "analogic",
  "complement",
  "analogic-complement",
  "triad",
  "quad",
]

//global array of colors
let colorsArr = []

//-----------bar-top
const colorPickerEl = document.querySelector("#color-picker")
const schemesSelectEl = document.querySelector("#schemes-select")
const getSchemeBtn = document.querySelector(".get-scheme-btn")
const colorNumberEl = document.querySelector("#number-input")


//-----------initial values
let selectedColor = colorPickerEl.value.substring(1);
let selectedMode = modesEnum[0];
let selectedNumber = colorNumberEl.value

//------------Bind colors to the dom
const renderSelectItems = () => {
  let html = "";
  for (let item of modesEnum) {
    html += `<option value="${item}">${item}</option>`
  }
  schemesSelectEl.innerHTML = html;
};
//-----------assign selected value to selectedMode variable
schemesSelectEl.addEventListener(
  "change",
  (e) => (selectedMode = e.target.value)
);
//------------assign selected number of colors to selectedNumber variable
colorNumberEl.addEventListener("change", (e) => selectedNumber = e.target.value )

//------------sort items from the api returning only the hexadecimal value
const sortColors = (data) => {
  colorsArr = [];
  for (let item of data.colors) {
    colorsArr.push(item.hex.value)
  }
  renderColorBar()
};
//-------------Query
const getScheme = () => {
  fetch(
    `https://www.thecolorapi.com/scheme?hex=${selectedColor}&mode=${selectedMode}&count=${selectedNumber}`
  )
    .then((res) => res.json())
    .then((data) => {
      colorsArr = sortColors(data)
    });
};

//-------------calls getScheme
getSchemeBtn.addEventListener("click", getScheme);

//-------------gets the picker value and cut the "#"
colorPickerEl.addEventListener("change", (e) => {
  selectedColor = e.target.value.substring(1);
});

//----------color-bars
const colorBarsEl = document.querySelector(".color-bars");
const colorBarEl = document.querySelector(".color-bar");

const renderColorBar = () => {
  let html = "";
  
  for (let color of colorsArr) {
    html += `
            <div class="color-bar" style="background: ${color}" onclick="selectDiv('${color}')" >
                <label class="hexa" id="${color}" onclick="selectDiv('${color}')">${color}</label>
            </div>
            `;
  }
  colorBarsEl.innerHTML = html;
};

//------------Copy to clipboard
const selectDiv = (data) => {
  navigator.clipboard.writeText(data)
}

//-------------Render App
const renderApp = () => {
  renderSelectItems();
  getScheme();
};

renderApp();
